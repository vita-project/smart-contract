const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledVita = require('./build/VitaToken.json');

const provider = new HDWalletProvider(
  //Don't bother, this was for testing:
  'empower silent goose clap isolate govern squirrel pole chaos before ship unveil',
  'https://rinkeby.infura.io/1slSSwYPKU653twoeicI'
);

const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();
  console.log('Attempting to deploy from account', accounts[0]);

  const result = await new web3.eth.Contract(JSON.parse(compiledVita.interface))
    .deploy({ data: compiledVita.bytecode })
    .send({ gas: '2000000', from: accounts[0] });

  console.log('Contract deployed to: ', result.options.address);
  
};
deploy();
