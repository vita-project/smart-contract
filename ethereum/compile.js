const path = require('path');
const fs = require('fs-extra');
const solc = require('solc');

const buildPath = path.resolve(__dirname, 'build');
fs.removeSync(buildPath);

const VitaPath = path.resolve(__dirname, 'contracts', 'Vita.sol');
const VitaFile = fs.readFileSync(VitaPath, 'utf8');

const output = solc.compile(VitaFile,1).contracts;

fs.ensureDirSync(buildPath);

for(let contract in output) {
  fs.outputJsonSync(
    path.resolve(buildPath, contract.replace(':','') + '.json'),
    output[contract]
  );
}
