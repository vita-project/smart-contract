import web3 from './web3';

import compiledFactory from './build/CampaignFactory.json';

export default web3.eth.Contract(
  JSON.parse(compiledFactory.interface),
  '0x72f924BafB83B61E2D9C9994d110d5F7C258752A'
);
