const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider()); // Connecting web3 to local blockchain test network ganache

const compiledVitaToken = require('../ethereum/build/VitaToken.json');
const compiledVitaReward = require('../ethereum/build/VitaReward.json');

let VitaToken;
let VitaReward;
let VitaReward2;

const timeTravel = function (time) {
  return new Promise((resolve, reject) => {
    web3.currentProvider.sendAsync({
      jsonrpc: "2.0",
      method: "evm_increaseTime",
      params: [time], // 86400 is num seconds in day
      id: new Date().getTime()
    }, (err, result) => {
      if(err){ return reject(err) }
      return resolve(result)
    });
  })
}

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  VitaToken = await new web3.eth.Contract(JSON.parse(compiledVitaToken.interface))
    .deploy({ data: compiledVitaToken.bytecode })
    .send({ from: accounts[0], gas: '5000000' });

  VitaReward = await new web3.eth.Contract(JSON.parse(compiledVitaReward.interface))
    .deploy({arguments: [VitaToken.options.address], data: compiledVitaReward.bytecode })
    .send({ from: accounts[0], gas: '2000000' });

  VitaReward2 = await new web3.eth.Contract(JSON.parse(compiledVitaReward.interface))
    .deploy({arguments: [VitaToken.options.address], data: compiledVitaReward.bytecode })
    .send({ from: accounts[0], gas: '2000000' });
  //Agregar VitaReward a Vita Token
  await VitaToken.methods.newVitaReward(VitaReward.options.address).send({
          from: accounts[0],
          gas: '1000000'
  });
});

describe('Vita token', () => {
  it('deploys the token contract correctly', () => {
    assert.ok(VitaToken.options.address);
  });
  it('deploys the reward contract correctly', () => {
    assert.ok(VitaReward.options.address);
  });
  it('VitaToken is owned by manager', async () => {
    let manager = await VitaToken.methods.manager().call()
    assert.equal(manager, accounts[0]);
  });
  it('VitaReward is owned by manager', async () => {
    let manager = await VitaReward.methods.manager().call()
    assert.equal(manager, accounts[0]);
  });
  it('VitaReward has VitaToken address', async () => {
    let vita_token = await VitaReward.methods.vita_token().call()
    assert.equal(VitaToken.options.address, vita_token);
  });
  it('VitaToken knows the correct reward amount', async () => {
    let rewardAmount = await VitaToken.methods.total_reward_amount().call();
    assert.equal(rewardAmount,((10 ** 28)/2));
  });
  it('VitaReward has the correct reward amount', async () => {
    let accountBalance = await VitaToken.methods.balanceOf(VitaReward.options.address).call();
    assert.equal(accountBalance,((10 ** 28)/2));
  });
  it('changing vita reward contract updates its amount', async () => {
      //Agregar VitaReward a Vita Token
      await VitaToken.methods.newVitaReward(VitaReward2.options.address).send({
              from: accounts[0],
              gas: '1000000'
      });
      let rewardAmount = await VitaToken.methods.balanceOf(VitaReward2.options.address).call();
      assert.equal(rewardAmount,((10 ** 28)/2));
  });
  it('doing a reward updates balance of company, patient, manager and VitaReward', async () => {
      //Mil vitas para el paciente (5% para la compañía y 5% para vita)
      let reward = 1000;
      let patient = accounts[1];
      let company = accounts[2];
      let done = await VitaReward.methods.evaluateReward(reward, patient, company).send({
               from: accounts[0],     
               gas: '1000000'
      });
      let managerBalance = await VitaToken.methods.balanceOf(accounts[0]).call();
      let patientBalance = await VitaToken.methods.balanceOf(patient).call();
      let companyBalance = await VitaToken.methods.balanceOf(company).call();
      let rewardBalance = await VitaToken.methods.balanceOf(VitaReward.options.address).call();
      assert.ok(done);
      assert.equal(managerBalance,((10 ** 28)/2 + (reward * 10 ** 18)*0.05));
      assert.equal(companyBalance,(reward * 10 ** 18)*0.05);
      assert.equal(patientBalance,(reward * 10 ** 18)*0.9);
      assert.equal(rewardBalance,((10 ** 28)/2 - (reward * 10 ** 18)));
  });
  it('manager has the correct balance', async () => {
    let managerBalance = await VitaToken.methods.balanceOf(accounts[0]).call();
    assert.equal(managerBalance,((10 ** 28)/2));
  });
  it('a new account has balance 0', async () => {
    let accountBalance = await VitaToken.methods.balanceOf(accounts[1]).call();
    assert.equal(accountBalance,0);
  });
  it('transfers money from manager to account 1', async () => {
    await VitaToken.methods.transfer(
      accounts[1],
      (1 * (10 ** 18))
    ).send({
      from: accounts[0],
      gas: '1000000'
    });
    let account1Balance = await VitaToken.methods.balanceOf(accounts[1]).call();
    assert.equal(account1Balance,(1 * (10 ** 18)));
  });
  it('transfers mone from manager to account 1 and discounts money from manager', async () => {
    await VitaToken.methods.transfer(
      accounts[1],
      (1 * (10 ** 18))
    ).send({
      from: accounts[0],
      gas: '1000000'
    });
    let account0Balance = await VitaToken.methods.balanceOf(accounts[0]).call();
    assert.equal(account0Balance,(((10 ** 28)/2)) - (1 * (10 ** 18)));
  });
  it('crowdsale first transaction works', async () => {
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[2] , value: web3.utils.toWei("10", "ether"), gas: '1000000' })
    let balanceAccount = await VitaToken.methods.balanceOf(accounts[2]).call();
    assert.equal(balanceAccount, 1400000 * (10 ** 18));
  });
  it('Time travel to test all stages of crowdsale', async () => {
    let balanceAccount;
    timeTravel(86400 * 3);
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[2] , value: web3.utils.toWei("10", "ether"), gas: '1000000' })
    balanceAccount = await VitaToken.methods.balanceOf(accounts[2]).call();
    assert.equal(balanceAccount, 1400000 * (10 ** 18));
    timeTravel(86400 * 15);
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[3] , value: web3.utils.toWei("10", "ether"), gas: '1000000' })
    balanceAccount = await VitaToken.methods.balanceOf(accounts[3]).call();
    assert.equal(balanceAccount, 1350000 * (10 ** 18));
    timeTravel(86400 * 50);
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[4] , value: web3.utils.toWei("10", "ether"), gas: '1000000' })
    balanceAccount = await VitaToken.methods.balanceOf(accounts[4]).call();
    assert.equal(balanceAccount, 1200000 * (10 ** 18));
    timeTravel(86400 * 20);
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[1] , value: web3.utils.toWei("10", "ether"), gas: '1000000' })
    balanceAccount = await VitaToken.methods.balanceOf(accounts[1]).call();
    assert.equal(balanceAccount, 1100000 * (10 ** 18));
  })
  it('crowdsale sells 35 ETH and disccounts that from manager', async () => {
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[2] , value: web3.utils.toWei("10", "ether"), gas: '1000000' })
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[2] , value: web3.utils.toWei("10", "ether"), gas: '1000000' })
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[3] , value: web3.utils.toWei("10", "ether"), gas: '1000000' })
    let managerBalance = await VitaToken.methods.balanceOf(accounts[0]).call();
    assert.equal(managerBalance, ((10 ** 28)/2) - ((3 * 1400000) * (10 ** 18)));
  });
  // Este test solo servira cuando el max_crowd_vitas sea de 500.000, y comentar el de arriba
  /*it('stops crowdsale when reaching 3 billion', async () => {
    console.log("Obteniendo balance cuenta 1");
    let balance1 = await web3.eth.getBalance(accounts[1]);
    await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[2] , value: web3.utils.toWei("1", "ether"), gas: '1000000' })
    let collected_vitas = await VitaToken.methods.collected_crowd_vitas().call();
    let max_vita = await VitaToken.methods.max_crowd_vitas().call();
    console.log("Vitas collected: ", collected_vitas);
    console.log("Max Vita: ", max_vita);
    try {
      await web3.eth.sendTransaction({ to: VitaToken.options.address, from: accounts[2] , value: web3.utils.toWei("1", "ether"), gas: '1000000' })
      assert(false);
    } catch(e) {
      assert(true);
    }
  });*/
});
